#include <stdlib.h>
#include <stdio.h>

#ifdef DIRECT
#include <mimalloc.h>
#define malloc mi_malloc
#define free mi_free
#endif

int main() {
    int *c = malloc(4096);
    printf("%p\n", c);
    free(c);
    return 0;
}
