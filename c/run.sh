#!/usr/bin/env bash

set -ex

MIMALLOC=../mimalloc

build_mimalloc() {
    (
        cd $MIMALLOC
        nix run nixpkgs#cmake -- .
        make
    )
}

direct_obj() {
    # Link directly against mi_malloc, etc.
    gcc -c main.c -I$MIMALLOC/include -DDIRECT
    gcc main.o $MIMALLOC/mimalloc.o -o main
}

obj() {
    # Use malloc but link against mimalloc.c
    gcc -c main.c
    gcc main.o $MIMALLOC/mimalloc.o -o main
}

src() {
    # Use malloc but link against out own compilation of static.c
    gcc -c main.c
    gcc -c -I$MIMALLOC/include \
        -DMI_MALLOC_OVERRIDE  -DNDEBUG -fPIC -fvisibility=hidden -fno-builtin-malloc \
        $MIMALLOC/src/static.c -o static.o
    gcc main.o static.o -I$MIMALLOC/include -o main
}

src
#obj

MIMALLOC_SHOW_STATS=1 ./main
