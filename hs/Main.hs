{-# LANGUAGE ForeignFunctionInterface #-}

module Main where

import Foreign.Marshal.Alloc
import Foreign.C.Types
import Foreign.Ptr

foreign import ccall unsafe "malloc"
    c_malloc :: CSize -> IO (Ptr a)

foreign import ccall unsafe "free"
    c_free :: Ptr a -> IO ()

main :: IO ()
main = do
    putStrLn "Hello, Haskell!"
    a <- c_malloc (4096*64)
    c_free a

    putStrLn "Hello, Haskell!"

    b <- mallocBytes (4096*32)
    free b
    putStrLn "Hello, Haskell!"
